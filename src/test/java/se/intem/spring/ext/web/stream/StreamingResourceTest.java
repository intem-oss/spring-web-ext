package se.intem.spring.ext.web.stream;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

class StreamingResourceTest {

    @Test
    void should_clean_filename() {

        StreamingResource resource = new StreamingResource(MediaType.ALL)
            .withFilename("a/b:c<d>e\\f.pdf");

        assertThat(resource.getFilename(), equalTo("a_b_c_d_e_f.pdf"));
    }

}
