package se.intem.spring.ext.web.stream;

import com.google.common.base.Strings;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.util.MimeType;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

/**
 * Remember to put @ResponseBody on methods returning streaming resources.
 * <p>
 * Remember to add StreamingResourceHttpMessageConverter bean to application.
 */
@SuppressWarnings("unused")
public class StreamingResource {

    /*
     Content length must be either null or non-negative
     https://github.com/spring-projects/spring-framework/commit/ec055da7c3d939a867436821a1405835475a6393
    */
    private Long contentLength = null;
    private StreamingResponseBody streamable;
    private MediaType contentType = MediaType.APPLICATION_OCTET_STREAM;
    private String filename;

    public StreamingResource(final MediaType contentType) {
        this.contentType = contentType;
    }

    public StreamingResource(final String mimeType) {
        this.contentType = MediaType.valueOf(mimeType);
    }

    public StreamingResource(final MimeType mimeType) {
        this.contentType = MediaType.valueOf(mimeType.toString());
    }

    @Nullable
    public Long getContentLength() {
        return this.contentLength;
    }

    public void setContentLength(final Long contentLength) {
        this.contentLength = contentLength;
    }

    public StreamingResource withContentLength(Long contentLength) {
        this.contentLength = contentLength;
        return this;
    }

    public StreamingResource withContent(final StreamingResponseBody streamable) {
        this.streamable = streamable;
        return this;
    }

    public StreamingResponseBody getContent() {
        return streamable;
    }

    public MediaType getContentType() {
        return contentType;
    }

    public StreamingResource withFilename(final String filename) {
        this.filename = sanitizeFilename(filename);
        return this;
    }

    /**
     * https://docs.microsoft.com/sv-se/windows/win32/fileio/naming-a-file?redirectedfrom=MSDN#naming_conventions
     * <p>
     * https://stackoverflow.com/a/41108758
     */
    private String sanitizeFilename(String filename) {
        return Strings.nullToEmpty(filename).replaceAll("[\\\\/:*?\"<>|]", "_");
    }

    public boolean hasFilename() {
        return !Strings.nullToEmpty(filename).trim().isEmpty();
    }

    public String getFilename() {
        return this.filename;
    }
}
