package se.intem.spring.ext.web.mvc;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.MediaType;

/*-
 * Utility methods from
 * https://github.com/spring-projects/spring-mvc-showcase/blob/master/src/main/java/org/springframework/mvc/extensions/ajax/AjaxUtils.java
 */
@Deprecated
public class AjaxUtils {

    public static boolean isJsonRequest(final HttpServletRequest request) {
        String contentType = request.getContentType();
        MediaType requested = MediaType.valueOf(contentType);
        return MediaType.APPLICATION_JSON.includes(requested);
    }

    public static boolean isAjaxRequest(final HttpServletRequest request) {
        String requestedWith = request.getHeader("X-Requested-With");
        return "XMLHttpRequest".equals(requestedWith);
    }

    public static boolean isAjaxUploadRequest(final HttpServletRequest webRequest) {
        return webRequest.getParameter("ajaxUpload") != null;
    }

    private AjaxUtils() {
    }

}
