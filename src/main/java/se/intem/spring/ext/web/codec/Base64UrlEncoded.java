package se.intem.spring.ext.web.codec;

import com.google.common.io.BaseEncoding;
import org.slf4j.Logger;

import java.nio.charset.StandardCharsets;

import static org.slf4j.LoggerFactory.getLogger;

@SuppressWarnings("unused")
public class Base64UrlEncoded {

    /* Use separate encoding to handle slash and semicolon within wbs string. */
    private static final BaseEncoding encoder = BaseEncoding.base64Url().omitPadding();

    private String encoded;

    public Base64UrlEncoded() {
    }

    public Base64UrlEncoded(String encoded) {
        this.encoded = encoded;
    }

    public String getEncoded() {
        return encoded;
    }

    public void setEncoded(String encoded) {
        this.encoded = encoded;
    }

    public String getDecoded() {
        if (encoder.canDecode(encoded)) {
            String decoded = new String(encoder.decode(encoded), StandardCharsets.UTF_8);
            return decoded;
        } else {
            /* Handle urls which have not yet been converted to use base64url */
            log.warn("Unable to decode, returning as is: {}", encoded);
            return encoded;
        }

    }

    public static Base64UrlEncoded encode(String text) {
        return new Base64UrlEncoded(encoder.encode(text.getBytes(StandardCharsets.UTF_8)));
    }

    @Override
    public String toString() {
        return this.encoded;
    }

    /** Logger for this class */
    private static final Logger log = getLogger(Base64UrlEncoded.class);
}
