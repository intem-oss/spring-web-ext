package se.intem.spring.ext.web.stream;

import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@SuppressWarnings("unused")
public abstract class ZipStreamingOutput implements StreamingResponseBody {

    private ZipOutputStream zipOutput;
    private ZipEntry currentEntry;
    private Charset charset;

    public ZipStreamingOutput() {
        this(StandardCharsets.UTF_8);
    }

    @SuppressWarnings("WeakerAccess")
    public ZipStreamingOutput(final Charset charset) {
        this.charset = charset;
    }

    @Override
    public final void writeTo(final OutputStream output) throws IOException {

        this.zipOutput = new ZipOutputStream(output, charset);

        try {
            writeTo(zipOutput);
        } finally {
            zipOutput.flush();
            zipOutput.close();
            output.flush();
        }

    }

    protected abstract void writeTo(ZipOutputStream output) throws IOException;

    protected ZipEntry openEntry(final String filename) throws IOException {
        if (this.currentEntry != null) {
            throw new IllegalStateException("There is already an open entry. Close previous entry first.");
        }

        ZipEntry entry = new ZipEntry(filename);
        zipOutput.putNextEntry(entry);
        this.currentEntry = entry;
        return this.currentEntry;
    }

    protected void closeEntry() throws IOException {
        if (this.currentEntry == null) {
            throw new IllegalStateException("There is no open entry.");
        }
        zipOutput.closeEntry();
        this.currentEntry = null;
    }

}
