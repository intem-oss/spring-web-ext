package se.intem.spring.ext.web.ssl;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@SuppressWarnings("unused")
public class ProxiedSslSecurityFilter implements Filter {

    /** Logger for this class */
    private static final Logger log = LoggerFactory.getLogger(ProxiedSslSecurityFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse res,
                         FilterChain chain) throws IOException, ServletException {
        //log.trace("Filtering...");

        HttpServletRequest req = (HttpServletRequest) request;

        String forwardedProtocol = req.getHeader("X-Forwarded-Proto");
        //log.info("Forwarded protocol {}", forwardedProtocol);

        if ("https".equals(forwardedProtocol)) {
            ProxiedSslRequestWrapper fakereq = new ProxiedSslRequestWrapper(req);
            chain.doFilter(fakereq, res);
        } else {
            chain.doFilter(req, res);
        }
    }

    @Override
    public void destroy() {
    }
}
