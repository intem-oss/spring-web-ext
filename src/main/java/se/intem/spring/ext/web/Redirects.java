package se.intem.spring.ext.web;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SuppressWarnings("unused")
public class Redirects {

    public static <T> ResponseEntity<T> redirectTo(String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.LOCATION, url);
        return new ResponseEntity<>(headers, HttpStatus.FOUND); // = 302, which is what Stripes RedirectResolution uses
    }

}
