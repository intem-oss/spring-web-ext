package se.intem.spring.ext.web.ssl;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletRequestWrapper;

/**
 * Modifies request so that Spring Social will build correct URL for login redirection.
 */
public class ProxiedSslRequestWrapper extends HttpServletRequestWrapper {
    public ProxiedSslRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    public boolean isSecure() {
        return true;
    }

    public String getScheme() {
        return "https";
    }

    @Override
    public int getServerPort() {
        return 443;
    }

    /* Implementation copied from org.apache.catalina.core.ApplicationHttpRequest */
    @Override
    public StringBuffer getRequestURL() {

        StringBuffer url = new StringBuffer();
        String scheme = getScheme();
        int port = getServerPort();
        if (port < 0) {
            port = 80; // Work around java.net.URL bug
        }

        url.append(scheme);
        url.append("://");
        url.append(getServerName());
        if ((scheme.equals("http") && (port != 80))
            || (scheme.equals("https") && (port != 443))) {
            url.append(':');
            url.append(port);
        }
        url.append(getRequestURI());

        return (url);

    }

}
