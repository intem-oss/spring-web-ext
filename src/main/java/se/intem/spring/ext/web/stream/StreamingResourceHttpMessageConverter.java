package se.intem.spring.ext.web.stream;

import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.IOException;

/**
 * Should be added before more general message converters (such as MappingJackson2HttpMessageConverter).
 */
@SuppressWarnings("unused")
public class StreamingResourceHttpMessageConverter extends AbstractHttpMessageConverter<StreamingResource> {

    public StreamingResourceHttpMessageConverter() {
        super(MediaType.ALL);
    }

    @Override
    protected boolean supports(final Class<?> clazz) {
        return StreamingResource.class.isAssignableFrom(clazz);
    }

    @Override
    protected MediaType getDefaultContentType(final StreamingResource resource) {
        return resource.getContentType();
    }

    @Override
    protected Long getContentLength(final StreamingResource resource, final MediaType contentType) throws IOException {
        return resource.getContentLength();
    }

    @Override
    protected void writeInternal(final StreamingResource resource, final HttpOutputMessage outputMessage)
        throws IOException, HttpMessageNotWritableException {

        try {
            applyHeaders(resource, outputMessage);
            resource.getContent().writeTo(outputMessage.getBody());
        } finally {
            outputMessage.getBody().flush();
        }
    }

    protected void applyHeaders(final StreamingResource resource, final HttpOutputMessage outputMessage) {
        if (resource.hasFilename()) {
            ContentDisposition contentDisposition = ContentDisposition
                .attachment()
                .filename(resource.getFilename())
                .build();
            outputMessage.getHeaders().setContentDisposition(contentDisposition);
        }

        /*
         * Must explicitly set content type, otherwise first concrete content type from request's accept header will be
         * used.
         */
        outputMessage.getHeaders().setContentType(resource.getContentType());
    }

    @Override
    protected StreamingResource readInternal(final Class<? extends StreamingResource> clazz,
                                             final HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        throw new UnsupportedOperationException();
    }

}
